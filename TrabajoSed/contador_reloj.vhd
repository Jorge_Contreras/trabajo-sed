----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:50:14 12/12/2016 
-- Design Name: 
-- Module Name:    contador_reloj - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;


entity contador_reloj is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
				M1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Segundo digito del minuto.
				M0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0); --Primer digito del minuto.
				S1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0); --Segundo digito de los segundos.
				S0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)  --Primer digito de los segundos.
	);
end contador_reloj;

architecture Behavioral of contador_reloj is
	signal ss1: UNSIGNED(2 downto 0) := "000" ;
	signal ss0: UNSIGNED(3 downto 0) := "0000";
	signal mm1: UNSIGNED(2 downto 0) := "000" ;
	signal mm0: UNSIGNED(3 downto 0) := "0000";
begin
	reloj: process (clk, reset) begin
		if reset = '1' then
			mm1 <= "000" ;
			mm0 <= "0000";
			ss1 <= "000" ;
			ss0 <= "0000";
		elsif rising_edge(clk) then
			ss0 <= ss0 + 1;
			if ss0 = 9 then
				ss1 <= ss1 + 1;
				ss0 <= "0000";
			end if;
			-- Al pasar 59 segundos, contar un minuto.
			if ss1 = 5 AND ss0 = 9 then
				mm0 <= mm0 + 1;
				ss1 <= "000";
			end if;
			if mm0 = 9 then
				mm1 <= mm1 + 1;
				mm0 <= "0000";
			end if;
			-- Al pasar 59:59, regresar a 00:00.
			if mm1 = 5 AND mm0 = 9 AND ss1 = 5 AND ss0 = 9 then
				mm1 <= "000";
				mm0 <= "0000";
			end if;
		end if;
	end process;
	
	--Asignaci�n de se�ales.
	M1 <= STD_LOGIC_VECTOR(mm1);
	M0 <= STD_LOGIC_VECTOR(mm0);
	S1 <= STD_LOGIC_VECTOR(ss1);
	S0 <= STD_LOGIC_VECTOR(ss0);

end Behavioral;

