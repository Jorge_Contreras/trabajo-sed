----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:20:31 12/12/2016 
-- Design Name: 
-- Module Name:    reloj - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Descripci�n: 
--   Une el contador del reloj con los divisores de frecuencia y el controlador 
--   de siete segmentos completo para mostrar la hora en una tarjeta Basys2.
entity reloj is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           salida : out  STD_LOGIC_VECTOR(7 downto 0);
           MUX : out  STD_LOGIC_VECTOR(3 downto 0)
		);
end reloj;

architecture Behavioral of reloj is
	COMPONENT clk1Hz IS
		PORT (
            entrada: IN  STD_LOGIC;
            reset  : IN  STD_LOGIC;
            salida : OUT STD_LOGIC
        );
		END COMPONENT;
		
	COMPONENT contador_reloj IS
		PORT (
			clk  : IN  STD_LOGIC;
			reset: IN  STD_LOGIC;
			M1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
			M0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
			S1   : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
			S0   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
		);
	END COMPONENT;
	
	COMPONENT siete_segmentos_completo IS
		PORT (
			clk   : IN  STD_LOGIC;
			reset : IN  STD_LOGIC;
			D0    : IN  STD_LOGIC_VECTOR(5 downto 0);
			D1    : IN  STD_LOGIC_VECTOR(5 downto 0);
			D2    : IN  STD_LOGIC_VECTOR(5 downto 0);
			D3    : IN  STD_LOGIC_VECTOR(5 downto 0);
			salida: OUT STD_LOGIC_VECTOR(7 downto 0);
			MUX   : OUT STD_LOGIC_VECTOR(3 downto 0)
		);
	END COMPONENT;
	
	signal clk_out : STD_LOGIC := '0';
	signal MM1, SS1: STD_LOGIC_VECTOR(2 downto 0);
	signal MM0,SS0: STD_LOGIC_VECTOR(3 downto 0);
	signal pMM1, pMM0, pSS1, pSS0: STD_LOGIC_VECTOR(5 downto 0);
begin
	--PORT MAPs necesarios para habilitar el reloj.
	clk_i: clk1Hz PORT MAP(clk, reset, clk_out);
	cnt_i: contador_reloj PORT MAP(clk_out, reset, MM1, MM0, SS1, SS0);
	seg_i: siete_segmentos_completo PORT MAP(clk, reset, pSS0, pSS1, pMM0, pSS1, salida, MUX);
	
	--Padding de las se�ales del contador para siete segmentos.
	pMM1 <= "000" & MM1;
	pMM0 <= "00"  & MM0;
	pSS1 <= "000" & SS1;
	pSS0 <= "00"  & SS0;

end Behavioral;

